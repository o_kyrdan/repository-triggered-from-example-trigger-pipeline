import unittest
from mymath import core


class MyMathTest(unittest.TestCase):
    def test_sum_of_two(self):
        self.assertEqual(core.sum_of_two(2,3), 5)


if __name__ == '__main__':
    unittest.main()
